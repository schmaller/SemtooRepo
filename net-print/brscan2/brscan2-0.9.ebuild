# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

DESCRIPTION="Brother scanner driver type 2 for x86 and amd64 from Brother homepage"
HOMEPAGE="http://support.brother.com/g/b/downloadlist.aspx?c=de&lang=de&prod=mfc260c_eu_as&os=128&flang=English"
LICENSE="Brother-EULA"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="media-gfx/sane-backends[usb]
	dev-libs/libusb-compat"

src_unpack() {
	# fix source path as subdir ${P} is not created automatically
	S=${WORKDIR}
	if use x86; then
		local DEBFILE=${FILESDIR}/brscan2-0.2.5-1.i386.deb
	elif use amd64; then
		local DEBFILE=${FILESDIR}/brscan2-0.2.5-1.amd64.deb
	fi
	cd "${T}"
	ar -x "${DEBFILE}" 
}

src_install() {
	cd "${D}"
	unpack ./../temp/data.tar.gz
}

pkg_postinst() {
	/usr/local/Brother/sane/setupSaneScan2 -i
	einfo "Please remember to put all users you want to be able to use the scanner into group <lp>."
	einfo "Maybe it's neccessary to add them to group <usb>, too."
}

pkg_prerm() {
	/usr/local/Brother/sane/setupSaneScan2 -e
}
