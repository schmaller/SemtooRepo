EAPI=4

DESCRIPTION="Brother MFC-260C LPR+cupswrapper drivers"
HOMEPAGE="http://support.brother.com/g/b/producttop.aspx?c=gb&lang=en&prod=mfc260c_eu_as"
SRC_URI="http://download.brother.com/welcome/dlf006076/mfc260clpr-1.0.1-1.i386.deb
		 http://download.brother.com/welcome/dlf006078/mfc260ccupswrapper-1.0.1-1.i386.deb"

inherit multilib

FEATURES='sandbox'
LICENSE="Brother-EULA"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""
RESTRICT="strip"

RDEPEND="net-print/cups
		app-text/a2ps"


S="${WORKDIR}" # Portage will bitch about missing $S so lets pretend that we have vaild $S.

src_unpack() {
	for f in ${A}; do
		ar -x ../distdir/"$f"
		unpack ./data.tar.gz
	done
}

src_prepare() {
	# Patching generator/installation script
	sed -i 's#/usr/local/Brother#${S}/usr/local/Brother#g' "${S}/usr/local/Brother/Printer/mfc260c/cupswrapper/cupswrappermfc260c"
	sed -i 's#/usr/share/cups/model#${S}/usr/share/cups/model#g' "${S}/usr/local/Brother/Printer/mfc260c/cupswrapper/cupswrappermfc260c"
	sed -i 's#/usr/lib/cups/filter#${S}/usr/libexec/cups/filter#g' "${S}/usr/local/Brother/Printer/mfc260c/cupswrapper/cupswrappermfc260c"
	sed -i 's#/usr/share/ppd#/tmp#g' "${S}/usr/local/Brother/Printer/mfc260c/cupswrapper/cupswrappermfc260c"
	sed -i -r 's#/etc/init.d/cups(\w)*#true#g' "${S}/usr/local/Brother/Printer/mfc260c/cupswrapper/cupswrappermfc260c"
	sed -i 's#lpinfo#true#g' "${S}/usr/local/Brother/Printer/mfc260c/cupswrapper/cupswrappermfc260c"
	sed -i 's#lpadmin#true#g' "${S}/usr/local/Brother/Printer/mfc260c/cupswrapper/cupswrappermfc260c"

	# Execute it
	mkdir -p "${S}/usr/local/Brother/Printer"
	mkdir -p "${S}/usr/share/cups/model"
	mkdir -p "${S}/usr/libexec/cups/filter"
	"${S}/usr/local/Brother/Printer/mfc260c/cupswrapper/cupswrappermfc260c"

	# and repatch
	sed -i "s#${S}##g" "${S}/usr/libexec/cups/filter/brlpdwrappermfc260c"
}

src_install() {
	has_multilib_profile && ABI=x86

	dosbin "${WORKDIR}/usr/bin/brprintconf_mfc260c"

	mkdir -p ${D}/usr/local/Brother || die
	cp -r ${WORKDIR}/usr/local/Brother/* ${D}/usr/local/Brother/ || die

	mkdir -p ${D}/usr/libexec/cups/filter || die
	cp    ${WORKDIR}/usr/libexec/cups/filter/brlpdwrappermfc260c  ${D}/usr/libexec/cups/filter/ || die

	mkdir -p ${D}/usr/share/cups/model || die
	cp -r ${WORKDIR}/usr/share/cups/model/* ${D}/usr/share/cups/model/ || die
}

pkg_postinst() {

    setBrotherVars

	port2=`lpinfo -v | grep -i 'usb://Brother/${device_name}' | head -1`
	if [ "$port2" = '' ];then 
	    port2=`lpinfo -v | grep 'usb://Brother' | head -1`
    fi
	    
    if [ "$port2" = '' ]; then
		port2=`lpinfo -v | grep 'usb://' | head -1`
	fi
	port=`echo $port2| sed s/direct//g`
	        
	if [ "$port" = '' ];then
		port=usb:/dev/usb/lp0
	fi
	lpadmin -p ${printer_name} -E -v $port -m br${printer_model}.ppd

	ewarn "### You really wanna read this ###"
	elog "You need to use brprintconf_${printer_model} to change printer options"
	elog "For example, you should set paper type to A4 right after instalation"
	elog "or your prints will be misaligned!"
	elog
	elog "Set A4 Paper type:"
	elog "		brprintconf_${printer_model} -pt A4"
	elog "Set 'Fast Normal' quality:"
	elog "		brprintconf_${printer_model} -reso 300x300dpi"
	elog
	elog "For more options just execute brprintconf_${printer_model} as root"
	elog "You can check current settings in:"
	elog "		/usr/local/Brother/Printer/${printer_model}/inf/br${printer_model}rc"
	elog 
	elog "Also remember to restart cups !!"
}

pkg_prerm() {
    setBrotherVars
    lpadmin -x ${printer_name}
    
}

setBrotherVars() {
    printer_model="mfc260c"
    printer_name=`echo $printer_model | tr '[a-z]' '[A-Z]'`
    device_name=`echo $printer_name | eval sed -e 's/MFC/MFC-/' -e 's/DCP/DCP-/' -e 's/FAX/FAX-/'`
}
