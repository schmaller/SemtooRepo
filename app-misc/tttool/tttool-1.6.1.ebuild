# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$
# Written manually by dev@schmaller.de
#

EAPI=5

inherit haskell-cabal

DESCRIPTION="Working with files for the Tiptoi® pen"
HOMEPAGE="https://github.com/entropia/tip-toi-reveng"
SRC_URI="https://github.com/entropia/tip-toi-reveng/archive/${PV}.zip"

FEATURES='sandbox'

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

# FIXME dep and build for spool
DEPEND=">=dev-lang/ghc-7.5
	>=dev-haskell/cabal-1.10
 	>=dev-haskell/aeson-0.7
	>=dev-haskell/binary-0.7 <dev-haskell/binary-0.8
	>=dev-haskell/directory-1.2 <dev-haskell/directory-1.3
	>=dev-haskell/executable-path-0.0 <dev-haskell/executable-path-0.1
	>=dev-haskell/hashable-1.2 <dev-haskell/hashable-1.3
	>=dev-haskell/hpdf-1.4.10 <dev-haskell/hpdf-1.5
	>=dev-haskell/juicypixels-3.2.8	<dev-haskell/juicypixels-3.3
	>=dev-haskell/haskeline-0.7 <dev-haskell/haskeline-0.8
	>=dev-haskell/mtl-2.2 <dev-haskell/mtl-2.3
	>=dev-haskell/optparse-applicative-0.12
	>=dev-haskell/parsec-3.1 <dev-haskell/parsec-3.2
	>=dev-haskell/process-conduit-1.1 <dev-haskell/process-conduit-1.5
	>=dev-haskell/random-1.0 <dev-haskell/random-1.2
	>=dev-haskell/split-0.2 <dev-haskell/split-0.3
	>=dev-haskell/spool-0.1 <dev-haskell/spool-0.2
	>=dev-haskell/vector-0.10 <dev-haskell/vector-0.12
	>=dev-haskell/yaml-0.8 <dev-haskell/yaml-0.9
	>=dev-haskell/zlib-0.5 <dev-haskell/zlib-0.7
"

RDEPEND=">=sys-libs/ncurses-5"

S="${WORKDIR}/tip-toi-reveng-${PV}"

src_prepare() {
	cabal_chdeps 'template-haskell >= 2.7 && < 2.11' 'template-haskell >= 2.7' \
		     'optparse-applicative == 0.12.*'	'optparse-applicative >= 0.12' \
		     'aeson       >= 0.7     &&   < 0.12'	'aeson >= 0.7'
}
